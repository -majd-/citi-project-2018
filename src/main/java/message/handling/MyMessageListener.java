package message.handling;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.xml.parsers.DocumentBuilderFactory;

import citi.trading.trading.Trader;

public class MyMessageListener implements MessageListener {

	@SuppressWarnings("unchecked")
	public void onMessage(Message m) {
		System.out.println( "In Listener");
				
		TextMessage message = (TextMessage) m;
		try {
     		System.out.println(message.getText());
//			PrintStream out = new PrintStream(new FileOutputStream("output.txt"));
//			System.setOut(out);
  //   		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
     		AtomicLong i =new AtomicLong(23);
        	HashMap<String,String> ha = new HashMap<String,String>();
        	ha.put("long", "2");
        	ha.put("short","1");
     
        	Trader tr = new Trader("Apple","twoMA",500,ha,i);
     		tr.trListener(message.getText());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
