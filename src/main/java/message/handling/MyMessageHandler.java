package message.handling;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

public class MyMessageHandler {
	
	private ApplicationContext context;
	private JmsTemplate jmsTemplate;
	private Destination destination;
	private Destination replyTo;
	
	public MyMessageHandler() {
		context = new ClassPathXmlApplicationContext("spring-beans.xml");
		destination = (Destination) context.getBean("destination", Destination.class);
		replyTo = (Destination) context.getBean("replyTo", Destination.class);
		jmsTemplate = (JmsTemplate) context.getBean("messageSender");
	}


	public static void main(String[] args) {
		MyMessageHandler mmh = new MyMessageHandler();
		mmh.run();
	}

	private void run() {
		String time = OffsetDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		 String text = "<trade>\r\n" + 
					"  <buy>true</buy>\r\n" + 
					"  <id>0</id>\r\n" + 
					"  <price>88.0</price>\r\n" + 
					"  <size>2000</size>\r\n" + 
					"  <stock>HON</stock>\r\n" + 
					"  <whenAsDate>"+ time + "</whenAsDate>\r\n" + 
					"</trade>";
		makeMessage(text);
		System.out.println("Message Sent to JMS Queue:- ");

	}

	public void makeMessage(final String trade) {
		jmsTemplate.send(destination, new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				 String time = OffsetDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
//				 String text = "<trade>\r\n" + 
//						"  <buy>true</buy>\r\n" + 
//						"  <id>0</id>\r\n" + 
//						"  <price>88.0</price>\r\n" + 
//						"  <size>2000</size>\r\n" + 
//						"  <stock>HON</stock>\r\n" + 
//						"  <whenAsDate>"+ time + "</whenAsDate>\r\n" + 
//						"</trade>";
				TextMessage tm = session.createTextMessage();
				tm.setText(trade);
				/*MapMessage message = session.createMapMessage();
				message.setString("Hello", "World");
				message.setString("country", "India");
				message.setString("state", "Maharashtra");
				message.setString("city", "Pune");
				message.setJMSReplyTo(replyTo);
				message.setJMSCorrelationID("123-99999");*/
				tm.setJMSCorrelationID(time);
				tm.setJMSReplyTo(replyTo);
				return tm;
			}	
			
		});

	}
}
/*		Map map = new HashMap();
map.put("Name", "Vimal");
map.put("Age", new Integer(45));

jmsTemplate.convertAndSend("/dynamicqueues/myqueue", map,
		new MessagePostProcessor() {
			public Message postProcessMessage(Message message)
					throws JMSException {
				message.setIntProperty("ID", 9999);
				message.setJMSCorrelationID("123-99999");
				message.setJMSReplyTo(replyTo);
				return message;
			}
		});
}
*/