package citi.trading.strategy;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public abstract class Strategy {
    @Id
    private String title;

    private String parameters;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }
}

