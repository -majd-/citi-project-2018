package citi.trading.strategy;

import java.util.LinkedList;
import java.util.List;

public class MovingAverage extends Strategy {
	private List<Double> buffer;
	private int size;
	
	public List<Double> getBuffer() {
		return buffer;
	}

	public void setBuffer(List<Double> buffer) {
		this.buffer = buffer;
	}

	public double average() throws Exception{
		//System.out.println(buffer.size());
		//System.out.println(buffer.get(buffer.size()-1));
		
		if(buffer.size() < size){
			throw new Exception();
		}
		double avg = 0;
		for(int i=0; i<size; i++){
			avg += buffer.get(i);
		}
		return avg/size;
	}

	public MovingAverage(int size) {
		this.buffer = new LinkedList<Double>();
		this.size = size;
		
	}
	
	public void push(double value){
		if(buffer.size() != size){
			buffer.add(value);
		}
		else{
			buffer.remove(0);
			buffer.add(value);
			System.out.println("");
		}
	}
}
