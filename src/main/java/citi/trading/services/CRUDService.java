package citi.trading.services;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;

public interface CRUDService<T> {
    public static class NotFoundException
            extends Exception
    {
        private Class<?> type;
        private int trxID;

        public NotFoundException (Class<?> type, int trxID)
        {
            super ("No " + type.getSimpleName () +
                    " found with trxID " + trxID + ".");

            this.type = type;
            this.trxID = trxID;
        }
        public Class<?> getType ()
        {
            return type;
        }

        public int getTrxID ()
        {
            return trxID;
        }
    }

    /**
     Dedicated exception for trxID conflicts.
     */
    public static class ConflictException
            extends Exception
    {
        private Object candidate;
        private int trxID;

        public ConflictException (Object candidate, int trxID)
        {
            super ("There is already a " +
                    candidate.getClass ().getSimpleName () +
                    " with trxID " + trxID + ".");

            this.candidate = candidate;
            this.trxID = trxID;
        }

        public Object getCandidate ()
        {
            return candidate;
        }

        public int gettrxID ()
        {
            return trxID;
        }
    }

    /**
     Returns the total number of objects.
     */
    @Transactional
    public long getCount ();

    /**
     Gets all instances of the managed class.
     */
    @Transactional
    public List<T> getAll ();

    /**
     Returns the object with the given trxID.
     */
    @Transactional
    public T getByTrxID (int trxID)
            throws NotFoundException;

    /**
     Adds the given object to the data set, with a generated trxID
     that is guaranteed to be unique.
     */
    @Transactional
    public T add (T newObject);

    /**
     Adds the given object to the list, with given "natural" trxID.
     */
    @Transactional
    public void add (int trxID, T newObject)
            throws ConflictException;

    /**
     Updates the object whose trxID matches the given object with the
     given object's state.
     */
    @Transactional
    public T update (T modifiedObject)
            throws NotFoundException;

    /**
     Removes the given object.
     */
    @Transactional
    public void remove (T oldObject)
            throws NotFoundException;

    /**
     Removes the object with the given trxID.
     */
    @Transactional
    public void removeByTrxID (int trxID)
            throws NotFoundException;

}
