package citi.trading.services;

import java.util.List;

public class JPAService<T> implements CRUDService<T> {

    public long getCount() {
        return 0;
    }

    public List<T> getAll() {
        return null;
    }

    public T getByTrxID(int trxID) throws NotFoundException {
        return null;
    }

    public T add(T newObject) {
        return null;
    }

    public void add(int trxID, T newObject) throws ConflictException {

    }

    public T update(T modifiedObject) throws NotFoundException {
        return null;
    }

    public void remove(T oldObject) throws NotFoundException {

    }

    public void removeByTrxID(int trxID) throws NotFoundException {

    }
}
