package citi.trading.dao;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import citi.trading.trading.Transactions;

@Repository
public class TransactionDAO {
	@Autowired
	private EntityManagerFactory emf;
	private EntityManager em;
	private EntityTransaction tx;
	
	public TransactionDAO(){
		
	}
	
	@PostConstruct
	public void init(){
		em = emf.createEntityManager();
		tx = em.getTransaction();
	}
	
	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	public EntityTransaction getTx() {
		return tx;
	}

	public void setTx(EntityTransaction tx) {
		this.tx = tx;
	}

	public Transactions loadTransactionByID(int trxID) {
		Transactions outcome = null;
		String sql = "SELECT T FROM Transactions T WHERE T.trxID = :id";
		TypedQuery<Transactions> q = em.createQuery(sql, Transactions.class);
		q.setParameter("id", trxID);
		outcome = q.getSingleResult();
		return outcome;
	}
	
	public int countTransactions(){
		// String sql = "SELECT COUNT(T) FROM Transactions T";
		// TypedQuery<Integer> query = em.createQuery(sql, Integer.class);
		// int outcome = query.getSingleResult();
		// return outcome;
		CriteriaBuilder qb = entityManager.getCriteriaBuilder();
CriteriaQuery<Long> cq = qb.createQuery(Long.class);
cq.select(qb.count(cq.from(Transactions.class)));
//ParameterExpression<Integer> p = qb.parameter(Integer.class);
return entityManager.createQuery(cq).getSingleResult();

	}
	
	// unchecked date in format yyyy-MM-dd
	public List<Transactions> loadTransactionByDate(Date date) {
		TypedQuery<Transactions> q = em.createQuery("SELECT T FROM Transactions T WHERE FUNCTION('TRUNC', T.timeStamp) = :time", Transactions.class);
		q.setParameter("time", date);
		List<Transactions> trxList = q.getResultList();
		return trxList;
	}
	
	// unchecked
	public List<Transactions> loadTransactionByStrategy(String Strategy) {
		String sql = "SELECT T FROM Transactions T WHERE T.strategy = :name";
		TypedQuery<Transactions> q = em.createQuery(sql, Transactions.class);
		q.setParameter("name", Strategy);
		List<Transactions> trxList = q.getResultList();
		return trxList;
	}
	
	// unchecked
	public List<Transactions> loadAll(){
		String sql = "SELECT T FROM Transactions";
		TypedQuery<Transactions> query = em.createQuery(sql, Transactions.class);
		List<Transactions> trxList = query.getResultList();
		return trxList;
	}
	
	public void insertTransaction(Transactions trx) {
		tx.begin();
		em.persist(trx);
		tx.commit();
	}
	
	public void close() {
		em.close();
	}
}
