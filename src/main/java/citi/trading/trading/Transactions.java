package citi.trading.trading;

import java.util.Date;

import javax.persistence.*;

import citi.trading.strategy.Strategy;

import javax.persistence.Entity;

@Entity(name = "Transactions")
@Table(name = "Transactions")
public class Transactions {

    @PersistenceContext(name = "PersistenceUnit")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transactionID")
    private int trxID;

    @Column(name = "equityName")
    private String equityName;

    @Column(name = "transactionType")
    private String trxType;

    @Column(name = "equityAmount")
    private int equityAmount;

    @Column(name = "equityPrice")
    private double equityPrice;

    @Column(name = "currency")
    private String currency;

    @Column(name = "strategy")
    private String strategy;

    @Column(name = "strategyParameters")
    private String strategyParameters;

    @Column(name = "profit")
    private double profit;

    @Column(name = "timeStamp")
    @Temporal(TemporalType.DATE)
    private Date timeStamp;

    public int getTrxID() {
        return trxID;
    }

    public void setTrxID(int trxID) {
        this.trxID = trxID;
    }

    public String getEquityName() {
        return equityName;
    }

    public void setEquityName(String equityName) {
        this.equityName = equityName;
    }

    public String getTrxType() {
        return trxType;
    }

    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }

    public int getEquityAmount() {
        return equityAmount;
    }

    public void setEquityAmount(int equityAmount) {
        this.equityAmount = equityAmount;
    }

    public double getEquityPrice() {
        return equityPrice;
    }

    public void setEquityPrice(double equityPrice) {
        this.equityPrice = equityPrice;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getStrategy() {
        return strategy;
    }

    public void setStrategy(String strategy) {
        this.strategy = strategy;
    }

    public String getStrategyParameters() {
        return strategyParameters;
    }

    public void setStrategyParameters(String strategyParameters) {
        this.strategyParameters = strategyParameters;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}
