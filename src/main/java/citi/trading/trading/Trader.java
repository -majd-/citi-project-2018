package citi.trading.trading;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import citi.trading.dao.EquityDAO;
import citi.trading.dao.TransactionDAO;
import citi.trading.strategy.Strategy;
import citi.trading.strategy.TwoMovingAverage;
import message.handling.MyMessageHandler;

public class Trader {
	private String decision;
	private String stockSymbol;
	private Transactions trxObj;
	private String strategyName;
	private HashMap<String,Strategy> strategies;
	private TransactionDAO trxDAO;
	private boolean isTrading;
	private ArrayList<Double> stockPrices;
	private MyMessageHandler mmh = new MyMessageHandler();
	//private String parameters;
	private HashMap<String,String> parameters;
	private AtomicLong traderID;
	private double profit;
	private int amount;
	private int trxID;// TEMP
	private EquityDAO dataFetcher = new EquityDAO();
	private TwoMovingAverage tMA;



	public Trader(String stockSymbol, String strategyName, int amount,HashMap<String,String> parameters,AtomicLong tdID) {
		//super();
		//generate constructor
		isTrading = false;
		//strategies.put("TwoMovingAverage",Strategy TwoMovingAverage);
		this.parameters = parameters;
		stockPrices= new ArrayList<Double>();
		trxDAO = new TransactionDAO();
		traderID = tdID;
		this.strategyName = strategyName;
		this.amount = amount;
		this.stockSymbol = stockSymbol;
		tMA = new TwoMovingAverage(Integer.valueOf(parameters.get("long")),Integer.valueOf(parameters.get("short")));
		

	}
	//todo:
	//stategy parameter --
	//loss/profit use correlational id
	//kill/start
	//every 15 seconds fetch data --
	//xml parser--
	//return boolean confirmation to REST
	//amount buy --
	public String getDecision() {
		return decision;
	}
	public void setDecision(String decision) {
		this.decision = decision;
	}
	public Transactions getTrxObj() {
		return trxObj;
	}
	public void setTrxObj(Transactions trxObj) {
		this.trxObj = trxObj;
	}
//	public Strategy getStrategies() {
//		return strategy;
//	}
//	public void setStrategies(Strategy strategy) {
//		this.strategy = strategy;
//	}
	public TransactionDAO getTrxDAO() {
		return trxDAO;
	}
	public void setTrxDAO(TransactionDAO trxDAO) {
		this.trxDAO = trxDAO;
	}
	public void setIsTrading(boolean s){
		this.isTrading = s;
	}
	public boolean getIsTrading(){
		return isTrading;
	}
	public boolean checkInput(){
		//check input time valid (no negative)
		//profit&loss <100 (no negative)
		//amount >0
//		if(Integer.parseInt(parameters.get(short))<0||){


//		}
		return false;

	}

	public String runTwoMA(String longTime, String shortTime, double price)throws Exception{

		System.out.println(price);
	
	//	System.out.println("hoho");

		this.decision = tMA.crossover(price);
System.out.print(decision);
		return decision;



	}
	public void buy(int  id, double price){

		 String time = OffsetDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
				 String text = "<trade>\r\n" + 
						"  <buy>true</buy>\r\n" + 
						"  <id>"+id+"</id>\r\n" + 
						"  <price>"+price+"</price>\r\n" + 
						"  <size>"+amount+"</size>\r\n" + 
						"  <stock>"+stockSymbol+"</stock>\r\n" + 
						"  <whenAsDate>"+ time + "</whenAsDate>\r\n" + 
						"</trade>";
				mmh.makeMessage(text);
				System.out.println(text);

	}
	public void sell(int  id, double price){
		String time = OffsetDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
				 String text = "<trade>\r\n" + 
						"  <buy>false</buy>\r\n" + 
						"  <id>+id+</id>\r\n" + 
						"  <price>"+price+"</price>\r\n" + 
						"  <size>"+amount+"</size>\r\n" + 
						"  <stock>"+stockSymbol+"</stock>\r\n" + 
						"  <whenAsDate>"+ time + "</whenAsDate>\r\n" + 
						"</trade>";
				mmh.makeMessage(text);
				System.out.println(text);


	}
	public void trade()throws Exception {



		
		dataFetcher.readStockData(stockSymbol,10);//Integer.valueOf(parameters.get("long")));
		//we first load a list of data prices
		//System.out.println(((HashMap<String,Equity>) dataFetcher.getEquities()).containsKey(stockSymbol));
		//System.out.println(((ArrayList<Equity>) (dataFetcher.getEquities().get(stockSymbol))).size());
		
		for (int i = 0;i<((ArrayList<Equity>) (dataFetcher.getEquities().get(stockSymbol))).size();i++ ) {
			//System.out.println((((ArrayList<Equity>) dataFetcher.getEquities().get(stockSymbol)).get(i).getClosePrice()));
			stockPrices.add(new Double(((ArrayList<Equity>) dataFetcher.getEquities().get(stockSymbol)).get(i).getClosePrice()));
			//stockPrices.add(new Double(12.3));
		}

		//run first round data with strategy

		for (Double d:stockPrices) {
		//	System.out.println(parameters.get("long"));
			decision = runTwoMA(parameters.get("long"),parameters.get("short"),Double.valueOf(d));
		///	System.out.println(d);
			if(decision.equals("BUY")){
		
				buy(trxID,d);
				trxID++;
				decision = " ";
			///////// WHAT IS ID?

			}
			else if(decision.equals("SELL")){
				 
				sell(trxID,d);
				trxID++;
				decision = " ";


			}
			else{
				decision = " ";
				continue;
			}

		}
	
//		Runnable helloRunnable = new Runnable() {
//   			public void run() {
//   				try {
//   					//reset decision
//   					decision = " ";
//   					System.out.println("hello");
//					dataFetcher.readStockData(stockSymbol,2);
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//   				int size =((ArrayList<Equity>) dataFetcher.getEquities().get(stockSymbol)).size();
//   				double newPrice = ((ArrayList<Equity>) dataFetcher.getEquities().get(stockSymbol)).get(size-1).getClosePrice();
//				//get close price for given stockSymbol
//			try {
//				decision = runTwoMA(parameters.get("long"),parameters.get("short"), new Double(((ArrayList<Equity>) dataFetcher.getEquities().get(stockSymbol)).get(size-1).getClosePrice()));
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//			
//			if(decision.equals("BUY")){
//		
//				buy(trxID,newPrice);
//				trxID++;
//			}
//			else if(decision.equals("SELL")){
//				 
//				sell(trxID,newPrice);
//			}
//			else{
//				
//			}
//   			}
//		};
//		int count=0;
//		isTrading=true;
//
//		while(isTrading&&count<100){
//			ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
//			executor.scheduleAtFixedRate(helloRunnable, 0, 15, TimeUnit.SECONDS);
//			//if()//TODO:calculate profit and shut down  thread
//			System.out.println(count);
//			count++;
//		}
//
//		
	}

	//Receive msg and write it into database

	public void trListener(String msg){
		 try {
		 Transactions tr = new Transactions();
        DocumentBuilderFactory dbf =
        DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(msg));

        Document doc = db.parse(is);
        NodeList nodes = doc.getElementsByTagName("trade");

        
       // for (int i = 0; i < nodes.getLength(); i++) {
           Element element = (Element) nodes.item(0);

           NodeList res = element.getElementsByTagName("result");
           Element line = (Element) res.item(0);
           String result = getCharacterDataFromElement(line);
           System.out.println(result);

  		if(result.equals("FILLED")){
  			tr.setCurrency("USD");
  			tr.setStrategy("TwoMovingAverage");

           NodeList buy = element.getElementsByTagName("buy");
           line = (Element) buy.item(0);
           tr.setTrxType(getCharacterDataFromElement(line));
           System.out.println(getCharacterDataFromElement(line));


           NodeList id = element.getElementsByTagName("id");
           line = (Element) id.item(0);
          // System.out.println("Title: " + getCharacterDataFromElement(line));
           tr.setTrxID(Integer.valueOf(getCharacterDataFromElement(line)));
           System.out.println(getCharacterDataFromElement(line));


           NodeList EquityName = element.getElementsByTagName("stock");
           line = (Element) EquityName.item(0);
           tr.setEquityName(getCharacterDataFromElement(line));
           System.out.println(getCharacterDataFromElement(line));

           
           NodeList price = element.getElementsByTagName("price");
           line = (Element) price.item(0);
           tr.setEquityPrice(Double.valueOf(getCharacterDataFromElement(line)));
           System.out.println(getCharacterDataFromElement(line));

           NodeList size = element.getElementsByTagName("size");
           line = (Element) size.item(0);
           tr.setEquityAmount(Integer.valueOf(getCharacterDataFromElement(line)));
           System.out.println(getCharacterDataFromElement(line));


           //timestamp
           SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");


           NodeList wADate = element.getElementsByTagName("whenAsDate");
           line = (Element)wADate .item(0);
           Date date = format.parse(getCharacterDataFromElement(line));
           tr.setTimeStamp(date);
           System.out.println(date);
           

           //parameters
          tr.setStrategyParameters(parameters.get("short")+parameters.get("long")+parameters.get("profit")+parameters.get("loss"));
        //   tr.setStrategyParameters("1234");
          tr.setProfit(99);
           trxDAO.insertTransaction(tr);

       }
       else if(result.equals("PARTIALLY_FILLED")){

       }
       else{

       }


    }
    catch (Exception e) {
        e.printStackTrace();
    }
   
    
  }

  public static String getCharacterDataFromElement(Element e) {
    Node child = e.getFirstChild();
    if (child instanceof CharacterData) {
       CharacterData cd = (CharacterData) child;
       return cd.getData();
    }
    return "?";
  }

	
}
